package ru.devray.day16;

import com.fasterxml.jackson.databind.ser.Serializers;
import io.restassured.RestAssured;
import org.testng.annotations.Test;

public class RapidApiGoogleTest extends BaseTest {
    @Test
    public void test() {

        String sentenceToGuessLanguage = "угадай какой это язык";

        RestAssured
                .given()
                .log().all()
                .baseUri("https://google-translate1.p.rapidapi.com/language/translate/v2")
                .header("X-RapidAPI-Key","fefa98be9fmshc734d4c6e37c66ap1bd873jsn3afb3e80f3b0")
                .header("X-RapidAPI-Host","google-translate1.p.rapidapi.com")
                .body(sentenceToGuessLanguage)
                .when()
                .post("/detect")
                .then()
                .log().all();
    }

    @Test
    public void test2() {

        String sentenceToGuessLanguage = "this is english language";

        String resultLanguage = RestAssured
                .given()
                .spec(basicSpec())
                .body(sentenceToGuessLanguage)
                .when()
                .post("/detect")
                .then()
                .log().all()
                .extract().jsonPath().get("data.detections..language");
    }

    @Test
    public void test3() {

        String sentenceToGuessLanguage = "this is english language";

        String resultLanguage = RestAssured
                .given()
                .spec(basicSpec())
                .body(sentenceToGuessLanguage)
                .when()
                .post("/detect")
                .then()
                .log().all()
                .extract().jsonPath().get("data.detections..language");
    }
}
