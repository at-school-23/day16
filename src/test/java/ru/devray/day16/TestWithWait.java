package ru.devray.day16;

import io.restassured.RestAssured;
import org.awaitility.Awaitility;
import org.hamcrest.Matchers;
import org.testng.annotations.Test;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

public class TestWithWait {
    @Test
    public void test() {
        Awaitility.await()
                .timeout(25, TimeUnit.SECONDS)
                .pollDelay(500, TimeUnit.MILLISECONDS)
                .until(() -> {
                    return RestAssured
                            .given()
                            .log().all()
                            .when()
                            .get("http://localhost:9099/fetch-slow-data")
                            .then()
                            .log().all()
                            .extract().jsonPath().get(".status").equals("loaded");
                });

//        RestAssured
//                .given()
//                .log().all()
//                .when()
//                .get("http://localhost:9099/fetch-slow-data")
//                .then()
//                .log().all()
//                .body(Matchers.containsString("loaded"));
    }
}
