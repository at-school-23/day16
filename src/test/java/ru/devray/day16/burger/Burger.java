package ru.devray.day16.burger;

import java.time.LocalDateTime;
import java.util.List;

public class Burger {

    LocalDateTime timeCooked;

    List<Ingredient> ingredients;

    public Burger(List<Ingredient> ingredients) {
        this.timeCooked = LocalDateTime.now();
        this.ingredients = ingredients;
    }

    @Override
    public String toString() {
        return "Burger{" +
               "timeCooked=" + timeCooked +
               ", ingredients=" + ingredients +
               '}';
    }
}
