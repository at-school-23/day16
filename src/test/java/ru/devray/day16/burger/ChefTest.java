package ru.devray.day16.burger;

import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static ru.devray.day16.burger.Ingredient.*;

public class ChefTest {

    @Test
    public void testCookBurger() {

        Package myPackage = new Package(new ArrayList<>(List.of(
                CHEESE,
                SALAD,
                TOMATO,
                MAYONNAISE,
                KETCHUP,
                BUN,
                BUN,
                BEEF,
                BUN
        )));

        Chef chef = new Chef("Vasya");

        if (chef.validatePackage(myPackage)) {
            Burger burger = chef.cookBurger(myPackage);
            System.out.println(burger);
        }

    }
}