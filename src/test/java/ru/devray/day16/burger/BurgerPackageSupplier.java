package ru.devray.day16.burger;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class BurgerPackageSupplier {

    private static Random random = new Random();

    public Package generateRandomPackage(int ingredientsCount) {
        Ingredient[] ingredientValues = Ingredient.values();
        List<Ingredient> ingredientsList = new ArrayList<>();

        for (int i = 0; i < ingredientsCount; i++) {
            int randomIngredientIndex = random.nextInt(ingredientValues.length);
            Ingredient ingredient = ingredientValues[randomIngredientIndex];
            ingredientsList.add(ingredient);
        }

        return new Package(ingredientsList);
    }

    public List<Package> generateRandomPackages(int countPackages) {
        int ingredientCount = random.nextInt(3, 12);
        List<Package> packages = new ArrayList<>();
        for (int i = 0; i < countPackages; i++) {
            packages.add(generateRandomPackage(ingredientCount));
        }
        return packages;
    }

}
