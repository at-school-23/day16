package ru.devray.day16.burger;

import java.util.List;

public class App {
    public static void main(String[] args) {
        BurgerPackageSupplier burgerPackageSupplier = new BurgerPackageSupplier();

        List<Package> packages = burgerPackageSupplier.generateRandomPackages(10);
        System.out.println(packages);

        System.out.println("-----------------");
        Chef chef = new Chef("Ramsay");
        List<Package> validatePackages = chef.validatePackages(packages);
        List<Burger> burgers = chef.cookBurgers(validatePackages);
        System.out.println(burgers);
    }
}
