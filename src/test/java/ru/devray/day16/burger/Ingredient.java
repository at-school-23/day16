package ru.devray.day16.burger;

public enum Ingredient {
    //огурцы, помидоры, котлета, сыр, кетчуп, майонез, булочка, салат.

    CUCUMBER,
    TOMATO,
    BEEF,
    CHEESE,
    KETCHUP,
    MAYONNAISE,
    BUN,
    SALAD;

}
