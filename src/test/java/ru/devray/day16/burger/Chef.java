package ru.devray.day16.burger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class Chef {

    String name;

    public Chef(String name) {
        this.name = name;
    }

    public List<Package> validatePackages(List<Package> packages) {
        //проверить в упаковке наличие минимально необходимого набора ингредиентов - двух булочек и одной котлетки
        List<Package> validatedPackages = new ArrayList<>();

        for (Package p : packages) {
            if (validatePackage(p)) {
                validatedPackages.add(p);
            }
        }

        return validatedPackages;
    }

    public boolean validatePackage(Package p) {
        int bunCount = Collections.frequency(p.ingredients, Ingredient.BUN);
        return (bunCount >= 2) && (p.ingredients.contains(Ingredient.BEEF));
    }

    public List<Burger> cookBurgers(List<Package> packages) {
        List<Burger> burgers = new ArrayList<>();
        for (Package p : packages) {
            burgers.add(cookBurger(p));
        }
        return burgers;
    }

    public Burger cookBurger(Package p) {
        //1) Сверху должна быть булочка и снизу должна быть булочка, иначе бургера не получится :(
        //   Если в упаковке вдруг оказалось три булочки - одна из булочек должна попасть в середину бургера.
        //0
        int indexOfBun = p.ingredients.indexOf(Ingredient.BUN);
        Collections.swap(p.ingredients, indexOfBun, 0);
        //last
        int indexOfSecondBun = p.ingredients.lastIndexOf(Ingredient.BUN);
        Collections.swap(p.ingredients, indexOfSecondBun, p.ingredients.size() - 1);
        //size()/2
        for (int i = 1; i < p.ingredients.size() - 1; i++) {
            Ingredient ingredient = p.ingredients.get(i);
            if (ingredient.equals(Ingredient.BUN)) {
                int middleIndex = p.ingredients.size() / 2;
                p.ingredients.remove(i);
                p.ingredients.add(middleIndex, ingredient);
            }
        }

        //2) Сыр всегда должен находиться сверху котлеты.
        if (p.ingredients.contains(Ingredient.CHEESE)) {
            int cheeseIndex = p.ingredients.indexOf(Ingredient.CHEESE);
            int beefIndex = p.ingredients.indexOf(Ingredient.BEEF);

            p.ingredients.remove(cheeseIndex);
            p.ingredients.add(beefIndex, Ingredient.CHEESE);
        }

        //3) Салат должен всегда находиться над нижней булочкой.
        if (p.ingredients.contains(Ingredient.SALAD)) {
            int saladIndex = p.ingredients.indexOf(Ingredient.SALAD);

            p.ingredients.remove(saladIndex);
            p.ingredients.add(1, Ingredient.SALAD);
        }

        return new Burger(p.ingredients);
    }
}
