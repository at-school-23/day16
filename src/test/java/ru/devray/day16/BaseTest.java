package ru.devray.day16;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.specification.RequestSpecification;
import org.testng.annotations.BeforeSuite;

public class BaseTest {

    String token;

    @BeforeSuite
    public void setUp() {
        //post
        token = "1234";
    }

    public static RequestSpecification basicSpec() {

        //post - login, pass -> token

        RequestSpecBuilder rsb = new RequestSpecBuilder()
                .log(LogDetail.ALL)
                .setBaseUri("https://google-translate1.p.rapidapi.com/language/translate/v2")

                .addHeader("X-RapidAPI-Key","fefa98be9fmshc734d4c6e37c66ap1bd873jsn3afb3e80f3b0")
                .addHeader("X-RapidAPI-Host","google-translate1.p.rapidapi.com");

        return rsb.build();
    }

}
