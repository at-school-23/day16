package ru.devray.day16.model;

@lombok.Data
public class ResponseUserData {
    public Data data;
    public Support support;
}
@lombok.Data
class Data{
    public int id;
    public String email;
    public String first_name;
    public String last_name;
    public String avatar;
}
@lombok.Data
class Support{
    public String url;
    public String text;
}