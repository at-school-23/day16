package ru.devray.day16.model;

import lombok.Data;

import java.util.Objects;

@Data
public class UserData {
    private String username;
    private String password;
}
